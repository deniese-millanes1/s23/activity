//CRUD Operations

//Insert Documents (CREATE)
/*
	SYNTAX:
		Inserting One Document
			-db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			})
*/

db.users.insertOne ({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
})

/*
INSERT MANY DOCUMENTS
	
	SYNTAX:
		- db.collectionName.insertMany([
			{
				"fieldA": "valueA",
				"fieldB": "valueB"	
			},
			{
				"fieldA": "valueA",
				"fieldB": "valueB"	
			}
		])

*/

db.users.insertMany ([
		{
			"firstName": "Stephen",
			"lastName": "King",
			"age": 76,
			"email": "stephenking@mail.com",
			"department": "none"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 76,
			"email": "neilarmstrong@mail.com",
			"department": "none"
		},
	]);

//Mini Activity
/*
	1. Make a new collection with the name courses
	2. Insert the following fields and values
		name: JavaScript 101
		price: 5000
		description: Introduction to Javascript
		isActive: true

		name: HTML 101
		price: 2000
		description: Introduction to HTML
		isActive: true

		name: CS 101
		price: 2500
		description: Introduction to Javascript
		isActive: false

*/


db.courses.insertMany ([
		{
			"name": "JavaScript 101",
			"price": 5000,
			"description": "Introduction to Javascript",
			"isActive": true

		},
		{
			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true

		},
		{
			"name": "CS 101",
			"price": 2500,
			"description": "Introduction to CS",
			"isActive": false

		}
]);

//FIND DOCUMENTS (READ)
/*
	SYNTAX:
		- db.collectionName.find()							-will retrieve all our documents

		- db.collectionName.find({"criteria": "value"})		-will retrieve all documents that match our criteria

		- db.collectionName.findOne({"criteria": "value"})	-will return the first document in our collection that matched the criteria

		- db.collectionName.findOne({"criteria": "value"})	-will return the first document in our collection 
*/		


db.users.find();

db.users.find({"firstName": "Jane"});

db.users.find(
	{
		"lastName": "Armstrong",
		"age": 82
	}
);

//UPDATING DOCUMENTS (UPDATE)
/*
	SYNTAX:
	//Update One
		db.collectionName.updateOne(
			{
				"criteria": "field",
			},
			{
				$set: {
					"fieldToBeUpdate": "updateValue"
				}
			}
		)
	
	//Update Many
			db.collectionName.updateMany(
			{
				"criteria": "field",
			},
			{
				$set: {
					"fieldToBeUpdate": "updateValue"
				}
			}
		)
*/


db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@mail.com",
	"department": "none"
})


db.users.updateOne(
		{
			"firstName": "Test"
		},
		{
			$set: {
				"firstName": "Bill",
				"lastName": "Gates",
				"age": 65,
				"email": "billgates@mail.com",
				"department":"operations",
				"status": "active"
			}
		}

	);

//updating multiple documents
db.users.updateMany(
		{
			"department": "none"
		},
		{
			"$set": {
				"department": "HR"
			}
		}
	);

//REMOVING A FIELD
db.users.updateOne(
		{
			"firstName": "Bill"
		},
		{
			"$unset": {
				"status": "active"
			}
		}
	)


//Mini Activity
/*
	1. Update the HTML 101 Course
	2. Make the isActive to false
	3. Add enrollees field to all the documents in our courses collection
		Enrollees: 10

*/

db.courses.updateOne(
		{
			"name": "HTML 101"
		},
		{
			"$set": {
				"isActive": false,
				"Enrollees": 10
			}
		}
	);

db.courses.updateOne(
		{
			"name": "JavaScript 101",

		},
		{
			"$set": {
				"Enrollees": 10
			}
		}
	);


db.courses.updateOne(
		{
			"name": "CS 101",

		},
		{
			"$set": {
				"Enrollees": 10
			}
		}
	);

//EASIER WAY
db.courses.updateMany(
		{

		},
		{
			"$set": {
				"Enrollees": 10
			}
		}
	);	

//DELETING DOCUMENTS (DELETE)

//Document to delete
db.users.insertOne({
	"firstName": "Test"
});

//Deleting a single document
/*
	SYNTAX:
		- db.collectionName.deleteOne({"criteria":"value"})
*/

db.users.deleteOne({"firstName":"Test"});

//Deleting multiple document
/*
	SYNTAX:
		- db.collectionName.deleteMany({"criteria":"value"})
*/

db.users.deleteMany({"department":"HR"})

db.courses.deleteMany({})